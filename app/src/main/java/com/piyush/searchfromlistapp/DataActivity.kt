package com.piyush.searchfromlistapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        var getIntent= intent

        if (getIntent != null)
        {
            textView.text=getIntent.getStringExtra("tv")
            imageView.setImageResource(getIntent.getIntExtra("image",0))
        }


    }
}
