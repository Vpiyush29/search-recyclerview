package com.piyush.searchfromlistapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), android.widget.SearchView.OnQueryTextListener {

    var item = ArrayList<ExampleItem>()
    lateinit var myAdapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        imageData()

        myAdapter = RecyclerViewAdapter(item)
        recyclerView.adapter = myAdapter

        var mylayoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = mylayoutManager


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.filtre_menu, menu)

        var findItem = menu?.findItem(R.id.app_bar_search)

        var searchView = findItem?.actionView as android.widget.SearchView

        searchView.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {

        return false

    }

    override fun onQueryTextChange(newText: String?): Boolean {

        var filteredList = newText?.toLowerCase()
        var list = ArrayList<ExampleItem>()

        for (data in item) {
            var adi = data.text.toLowerCase()

            if (adi.contains(filteredList.toString())) {
                list.add(data)

            }
        }

        myAdapter.setFilter(list)

        return true
    }


    private fun imageData() {

        var resimler = arrayOf(
            R.drawable.ic_audio,
            R.drawable.ic_android,
            R.drawable.ic_sun,
            R.drawable.ic_audio,
            R.drawable.ic_android,
            R.drawable.ic_sun,
            R.drawable.ic_audio,
            R.drawable.ic_sun,
            R.drawable.ic_android,
            R.drawable.ic_audio

        )


        var isimler = arrayOf(

            "One 1",
            "Two 2",
            "Three 3",
            "Four 4",
            "Five 5",
            "Six 6",
            "Seven 7",
            "Eight 8",
            "Nine 9",
            "Ten 10"
        )

        for (i in 0..resimler.size - 1) {
            var eklenecekDost = ExampleItem(isimler[i], resimler[i])
            item.add(eklenecekDost)
        }


    }
}
