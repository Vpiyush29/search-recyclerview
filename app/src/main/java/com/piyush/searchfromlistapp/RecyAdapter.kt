package com.piyush.searchfromlistapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.example_layout.view.*

class RecyAdapter(listitem:ArrayList<ExampleItem>): RecyclerView.Adapter<RecyAdapter.RecyViewHolder>() {

    var dataList=listitem

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyViewHolder {



        var inflater=LayoutInflater.from(parent?.context)
        var inflateView=inflater.inflate(R.layout.example_layout,parent,false)
        return RecyViewHolder(inflateView)



    }

    override fun getItemCount(): Int {

        return dataList.size

    }

    override fun onBindViewHolder(holder: RecyViewHolder, position: Int) {

        var data=dataList.get(position)
        holder?.setData(data,position)


    }


    class RecyViewHolder(view: View):RecyclerView.ViewHolder(view){

        var cardView= view as CardView
        var dataText=cardView.tv
        var dataImage=cardView.img



        fun setData(poslist: ExampleItem, position: Int) {

            dataText.text=poslist.text
            dataImage.setImageResource(poslist.image)

        }


    }

}