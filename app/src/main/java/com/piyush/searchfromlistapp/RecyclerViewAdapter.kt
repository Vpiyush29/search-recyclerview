package com.piyush.searchfromlistapp

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.example_layout.view.*


class RecyclerViewAdapter(dataItem:ArrayList<ExampleItem>): RecyclerView.Adapter<RecyclerViewAdapter.DataViewHolder>() {

    var dataList=dataItem

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        var inflater=LayoutInflater.from(parent?.context)
        var inflateView=inflater.inflate(R.layout.example_layout,parent,false)
        return DataViewHolder(inflateView)

    }

    override fun getItemCount(): Int {

       return dataList.size
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {

        var positionData=dataList.get(position)
        holder?.setData(positionData,position)

    }


    inner class DataViewHolder(view: View):RecyclerView.ViewHolder(view) {

        var cardViewData=view as CardView

        var dostAd=cardViewData.tv
        var dostResim=cardViewData.img


        fun setData(positionItem: ExampleItem, position: Int) {

            dostAd.text=positionItem.text
            dostResim.setImageResource(positionItem.image)

            cardViewData.setOnClickListener { v-> var intent=Intent(v.context,DataActivity::class.java)

                intent.putExtra("tv",positionItem.text)
                intent.putExtra("image",positionItem.image)
                v.context.startActivity(intent)


            }

        }



    }

    fun setFilter(aranilanlar:ArrayList<ExampleItem>)
    {
        //sıfırladık listeyi
        dataList=ArrayList<ExampleItem>()

        //gelen listeyi atadı
        dataList.addAll(aranilanlar)
        notifyDataSetChanged()

    }




}